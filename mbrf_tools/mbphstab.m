function [out,P] = mbphstab(N,type)

% function out = mbphstab(N,type)
% 
%   Return optimal phases for phase-modulated or amplitude-modulated
%       low-peak power multiband pulses.
%
%   out: Optimal phases
%   P: matrix of optimal phases for selected type (optional)
%   N: Number of bands
%   type: 'phsmod', 'ampmod', or 'quadmod' (optional, default is 'phsmod'/Wong's phases)
% 
%   Will Grissom, Vanderbilt University, 2015

% for a given number of slices N, return Wong's optimal phases
if ~exist('type','var')
    type = 'phsmod';
end

switch type
    case 'phsmod'
        
        % Wong's phases: From E C Wong, ISMRM 2012, p. 2209
        
        if N < 3 || N > 16
            error 'N must be >= 3 and <= 16 for phase modulation phases'
        end
        
        P = zeros(14,15);
        P(1,1:2) =  [0.73 4.602]; % N = 3;
        P(2,1:3) =  [3.875 5.94 6.197];
        P(3,1:4) =  [3.778 5.335 0.872 0.471];
        P(4,1:5) =  [2.005 1.674 5.012 5.736 4.123];
        P(5,1:6) =  [3.002 5.998 5.909 2.624 2.528 2.440];
        P(6,1:7) =  [1.036 3.414 3.778 3.215 1.756 4.555 2.467];
        P(7,1:8) =  [1.250 1.783 3.558 0.739 3.319 1.296 0.521 5.332];
        P(8,1:9) =  [4.418 2.360 0.677 2.253 3.472 3.040 3.974 1.192 2.510];
        P(9,1:10) =  [5.041 4.285 3.001 5.765 4.295 0.056 4.213 6.040 1.078 2.759];
        P(10,1:11) = [2.755 5.491 4.447 0.231 2.499 3.539 2.931 2.759 5.376 4.554 3.479];
        P(11,1:12) = [0.603 0.009 4.179 4.361 4.837 0.816 5.995 4.150 0.417 1.520 4.517 1.729];
        P(12,1:13) = [3.997 0.830 5.712 3.838 0.084 1.685 5.328 0.237 0.506 1.356 4.025 4.483 4.084];
        P(13,1:14) = [4.126 2.266 0.957 4.603 0.815 3.475 0.977 1.449 1.192 0.148 0.939 2.531 3.612 4.801];
        P(14,1:15) = [4.359 3.510 4.410 1.750 3.357 2.061 5.948 3.000 2.822 0.627 2.768 3.875 4.173 4.224 5.941];
        
        out = [0 P(N-2,1:N-1)];
        
    case 'ampmod'
        
        % Malik's Hermitian phases: From S J Malik, ISMRM 2015, p. 2398
        
        if N < 4 || N > 12
            error 'N must be >= 4 and <= 12 for amplitude modulation phases'
        end
        
        P = zeros(9,12);
        P(1,1:4) = [0 pi pi 0]; % N = 4
        P(2,1:5) = [0 0 pi 0 0];
        P(3,1:6) = [1.691 2.812 1.157 -1.157 -2.812 -1.691];
        P(4,1:7) = [2.582 -0.562 0.102 0 -0.102 0.562 -2.582];
        P(5,1:8) = [2.112 0.220 1.464 1.992 -1.992 -1.464 -0.220 -2.112];
        P(6,1:9) = [0.479 -2.667 -0.646 -0.419 0 0.419 0.646 2.667 -0.479];
        P(7,1:10) = [1.683 -2.395 2.913 0.304 0.737 -0.737 -0.304 -2.913 2.395 -1.683];
        P(8,1:11) = [1.405 0.887 -1.854 0.070 -1.494 0 1.494 -0.070 1.854 -0.887 -1.405];
        P(9,1:12) = [1.729 0.444 0.722 2.190 -2.196 0.984 -0.984 2.196 -2.190 -0.722 -0.444 -1.729];
        
        out = P(N-3,1:N);
        
    case 'quadmod'
        
        % Grissom's quadratic phases (unpublished)
        
        k = 3.4/N; % quadratic phase coefficient
        out = k*(-(N-1)/2:(N-1)/2).^2; % phases for each band
        
    otherwise
        
        error 'Unrecognized type'
            
end