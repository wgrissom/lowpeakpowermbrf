%% Design and simulation parameters
tb = 3; % time-bandwidth product
slThick = 2; % mm
slSep = 20; % mm
pulseType = 'se'; % spin echo pulse
d1 = 0.01; % refocusing passband ripple
d2 = 0.01; % refocusing stopband ripple
maxB1 = 0.18; % Gauss, max RF amplitude
gMax = 4; % Gauss/cm, max gradient amplitude
gSlew = 20000; % Gauss/cm/s, max gradient slew
dt = 2e-6; % seconds, RF+gradient dwell time

gamma = 2*pi*4258; % radians/second/gauss

zSim = -12:0.01:12; % cm

%% Design pulses

disp 'Designing PINS/MultiPINS pulses...'

% Design a conventional PINS pulse
[rfPINS,gPINS,paramsPINS] = dz_pins(pulseType,d1,d2,tb,slSep,slThick,maxB1,gMax,gSlew,dt);

% Design a min-duration PINS pulse
minRFDur = true;
[rfPINSmin,gPINSmin,paramsPINSmin] = dz_pins(pulseType,d1,d2,tb,slSep,slThick,maxB1,gMax,gSlew,dt,minRFDur);

% Design a multiPINS pulse
minRFDur = false;
multiPINS = true;
Nb = 5; % MB factor 
[rfMultiPINS,gMultiPINS,paramsMultiPINS] = dz_pins(pulseType,d1,d2,tb,slSep,slThick,maxB1,gMax,gSlew,dt,minRFDur,multiPINS,Nb);

% Design a conventional PINS pulse (min-phase)
[rfPINSminphs,gPINSminphs,paramsPINSminphs] = dz_pins(pulseType,d1,d2,tb,slSep,slThick,maxB1,gMax,gSlew,dt,false,false,[],true);

% Design a multiPINS pulse (min-phase)
minRFDur = false;
multiPINS = true;
Nb = 5; % MB factor 
[rfMultiPINSminphs,gMultiPINSminphs,paramsMultiPINSminphs] = dz_pins(pulseType,d1,d2,tb,slSep,slThick,maxB1,gMax,gSlew,dt,minRFDur,multiPINS,Nb,true);


%% Simulate refocusing profiles

disp 'Simulating PINS/MultiPINS refocusing profiles...'

% Conventional PINS
[~,bPINS] = blochsim_simult(rfPINS(:)*dt*gamma,gPINS(:)*dt*gamma,zSim);
bPINS = bPINS.^2; % to get refocusing profile

% min-duration PINS
[~,bPINSmin] = blochsim_simult(rfPINSmin(:)*dt*gamma,gPINSmin(:)*dt*gamma,zSim);
bPINSmin = bPINSmin.^2; % to get refocusing profile

% MultiPINS
[~,bMultiPINS] = blochsim_simult(rfMultiPINS(:)*dt*gamma,gMultiPINS(:)*dt*gamma,zSim);
bMultiPINS = bMultiPINS.^2; % to get refocusing profile

% Conventional PINS (min-phase)
[~,bPINSminphs] = blochsim_simult(rfPINSminphs(:)*dt*gamma,gPINSminphs(:)*dt*gamma,zSim);
bPINSminphs = bPINSminphs.^2; % to get refocusing profile

% MultiPINS (min-phase)
[~,bMultiPINSminphs] = blochsim_simult(rfMultiPINSminphs(:)*dt*gamma,gMultiPINSminphs(:)*dt*gamma,zSim);
bMultiPINSminphs = bMultiPINSminphs.^2; % to get refocusing profile


%% Plot pulses and refocusing profiles

disp 'Plotting PINS/MultiPINS pulses and profiles...'

% time vectors for RF waveform plots
tPINS = ((0:length(rfPINS)-1)*dt-length(rfPINS)*dt/2)*1000;
tPINSmin = ((0:length(rfPINSmin)-1)*dt-length(rfPINSmin)*dt/2)*1000;
tMultiPINS = ((0:length(rfMultiPINS)-1)*dt-length(rfMultiPINS)*dt/2)*1000;
tPINSminphs = ((0:length(rfPINSminphs)-1)*dt-length(rfPINSminphs)*dt/2)*1000;
tMultiPINSminphs = ((0:length(rfMultiPINSminphs)-1)*dt-length(rfMultiPINSminphs)*dt/2)*1000;

% PINS versus min-dur PINS versus multiPINS
figure
subplot(311)
plot(tPINS,rfPINS);
xlabel 'ms',ylabel 'Gauss'
axis([min([tPINS tPINSmin tMultiPINS]) max([tPINS tPINSmin tMultiPINS]) [-1 1]*maxB1]);
title(sprintf('PINS: %0.3g ms duration',length(rfPINS)*dt*1000));
subplot(312)
plot(tPINSmin,rfPINSmin);
xlabel 'ms',ylabel 'Gauss'
axis([min([tPINS tPINSmin tMultiPINS]) max([tPINS tPINSmin tMultiPINS]) [-1 1]*maxB1]);
title(sprintf('PINS, min duration: %0.3g ms duration',length(rfPINSmin)*dt*1000));
subplot(313)
plot(tMultiPINS,rfMultiPINS);
xlabel 'ms',ylabel 'Gauss'
axis([min([tPINS tPINSmin tMultiPINS]) max([tPINS tPINSmin tMultiPINS]) [-1 1]*maxB1]);
title(sprintf('MultiPINS: %0.3g ms duration; M = %0.3g%%',length(rfMultiPINS)*dt*1000,paramsMultiPINS.M*100));

% linear- versus minimum-phase PINS and MultiPINS
figure
subplot(311)
plot(tPINS,rfPINS);
xlabel 'ms',ylabel 'Gauss'
axis([min([tPINS tPINSminphs tMultiPINSminphs]) max([tPINS tPINSminphs tMultiPINSminphs]) [-1 1]*maxB1]);
title(sprintf('PINS, linear phase: %0.3g ms duration',length(rfPINS)*dt*1000));
subplot(312)
plot(tPINSminphs,rfPINSminphs);
xlabel 'ms',ylabel 'Gauss'
axis([min([tPINS tPINSminphs tMultiPINSminphs]) max([tPINS tPINSminphs tMultiPINSminphs]) [-1 1]*maxB1]);
title(sprintf('PINS, min-phase: %0.3g ms duration',length(rfPINSminphs)*dt*1000));
subplot(313)
plot(tMultiPINSminphs,rfMultiPINSminphs);
xlabel 'ms',ylabel 'Gauss'
axis([min([tPINS tPINSminphs tMultiPINSminphs]) max([tPINS tPINSminphs tMultiPINSminphs]) [-1 1]*maxB1]);
title(sprintf('MultiPINS, min-phase: %0.3g ms duration; M = %0.3g%%',length(rfMultiPINSminphs)*dt*1000,paramsMultiPINSminphs.M*100));

% PINS versus min-dur PINS versus multiPINS
figure
subplot(311)
plot(zSim,abs(bPINS)*100);
xlabel 'cm',ylabel 'Refocusing Efficiency (%)'
axis([zSim(1) zSim(end) 0 100]);
title 'PINS'
subplot(312)
plot(zSim,abs(bPINSmin)*100);
title 'PINS, min duration'
xlabel 'cm',ylabel 'Refocusing Efficiency (%)'
axis([zSim(1) zSim(end) 0 100]);
subplot(313)
plot(zSim,abs(bMultiPINS)*100);
title 'MultiPINS'
xlabel 'cm',ylabel 'Refocusing Efficiency (%)'
axis([zSim(1) zSim(end) 0 100]);

% linear- versus minimum-phase PINS and MultiPINS
figure
subplot(311)
plot(zSim,abs(bPINS)*100);
xlabel 'cm',ylabel 'Refocusing Efficiency (%)'
axis([zSim(1) zSim(end) 0 100]);
title 'PINS, linear-phase'
subplot(312)
plot(zSim,abs(bPINSminphs)*100);
title 'PINS, min-phase'
xlabel 'cm',ylabel 'Refocusing Efficiency (%)'
axis([zSim(1) zSim(end) 0 100]);
subplot(313)
plot(zSim,abs(bMultiPINSminphs)*100);
title 'MultiPINS, min-phase'
xlabel 'cm',ylabel 'Refocusing Efficiency (%)'
axis([zSim(1) zSim(end) 0 100]);

