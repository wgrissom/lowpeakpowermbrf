function [rfMB,gMB,params] = dz_mbrf(pulseType,d1,d2,tb,Nb,slSep,slThick,maxB1,gSlew,n,phsOpt)

% function [rfMB,gMB,params] = dz_mbrf(pulseType,d1,d2,tb,Nb,slSep,slThick,maxB1,gSlew,phsOpt)
% 
%   Design a multiband pulse with a given peak B1, with optional inter-band phases to reduce duration.
% 
%   out: 
%       rfMB,gMB: RF and gradient pulse waveforms
%       params: Parameter structure, containing
%               B: multiband modulation function
%               gRefMoment: refocusing gradient area
%               dt: dwell time
%               params.gAmp = gAmp; % gradient plateau amplitude (Gauss/cm)
%               params.nRampPts = nRampPts; % number of time points in the gradient ramps
%               dtMax: ~ maximum dwell time the pulse could be interpolated to without aliasing
% 
%   in: 
%       pulseType: For SLR ('se','ex','sat','inv')
%       d1,d2: For SLR (passband and stopband ripple)
%       tb: Time-bandwidth product
%       Nb: number of bands
%       slSep: Slice separation in mm
%       slThick: Slice thickness in mm 
%       maxB1: max RF amplitude in gauss
%       gSlew: max gradient slew in Gauss/cm/s
%       n: number of time points (optional)
%       phsOpt: optional inter-band phase shift 
%               (default is 'none'; see mbphstab.m for others)
% 
%   Will Grissom, Vanderbilt University, 2015

if ~exist('phsOpt','var')
    phsOpt = 'none';
end
if ~exist('n','var')
    n = 8192;
end

gambar = 4258; % Hz/Gauss

% get a high-res pulse
rf1bLR = real(dzrf(128,tb,pulseType,'ls',d1,d2));
% interpolate
rf1b = interp1((0:127)./127,rf1bLR,(0:n-1)./(n-1),'spline',0);
rf1b = rf1b./sum(rf1b)*sum(rf1bLR);

% get inter-band phase shifts to reduce peak b1
switch phsOpt
    case 'none'
        phs = zeros(1,Nb);
    otherwise
        phs = mbphstab(Nb,phsOpt);
end

% calculate and apply the multiband modulation function
bandSep = slSep/slThick*tb; % band separation (integer)
B = exp(1i*2*pi/n*(-n/2:n/2-1)'*((0:Nb-1)-(Nb-1)/2)*bandSep); 
B = sum(B*diag(exp(1i*phs)),2); % apply phase shifts and sum to get total modulation function
rfMB = B.*rf1b(:); % apply multiband modulation function to get multiband RF

if strcmp(phsOpt,'none') || strcmp(phsOpt,'ampmod')
    rfMB = real(rfMB); % can discard imaginary part for convenience
end

% calculate the dwell time for the peak b1
dt = max(abs(rfMB))/(maxB1*2*pi*gambar); % seconds

% convert to Gauss
rfMB = rfMB./max(abs(rfMB))*maxB1; 

% calculate the gradient waveform amplitude
gAmp = tb/(dt*n)/gambar/(slThick/10); % Gauss/cm

% put ramps on each end
nRampPts = ceil(gAmp/(dt/2*gSlew)); % number of time points on the ramps
gMB = [gAmp*(0:nRampPts-1)'/nRampPts;gAmp*ones(n,1);gAmp*(nRampPts-1:-1:0)'/nRampPts];
rfMB = [zeros(nRampPts,1);rfMB;zeros(nRampPts,1)];

params.B = B; % the multiband modulation function
params.gRefMoment = sum(gMB)*dt/2; % required gradient refocusing area (Gauss/cm*seconds)
params.dt = dt; % dwell time for peak B1 (seconds)
params.gAmp = gAmp; % gradient plateau amplitude (Gauss/cm)
params.nRampPts = nRampPts; % number of time points in the gradient ramps
params.dtMax = 1/(gambar*slSep/10*Nb*gAmp); % seconds; max dwell time the pulse could be interpolated to without aliasing


