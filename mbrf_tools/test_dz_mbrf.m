%% Design and simulation parameters
tb = 4; % time-bandwidth product
slThick = 2; % mm
slSep = 20; % mm
pulseType = 'se'; % spin echo pulse
d1 = 0.01; % refocusing passband ripple
d2 = 0.01; % refocusing stopband ripple
maxB1 = 0.18; % Gauss, max RF amplitude
gSlew = 20000; % Gauss/cm/s, max gradient slew
Nb = 5; % number of bands
gamma = 2*pi*4258; % radians/second/gauss
n = 512; % number of time points

zSim = -12:0.01:12; % cm


%% Design pulses

disp 'Designing MB pulses...'

% Design a conventional MB pulse
[rfMB,gMB,paramsMB] = dz_mbrf(pulseType,d1,d2,tb,Nb,slSep,slThick,maxB1,gSlew,n);

% Design a phase-optimized MB pulse (Wong)
[rfMBphs,gMBphs,paramsMBphs] = dz_mbrf(pulseType,d1,d2,tb,Nb,slSep,slThick,maxB1,gSlew,n,'phsmod');

% Design a phase-optimized MB pulse (Quadratic)
[rfMBquad,gMBquad,paramsMBquad] = dz_mbrf(pulseType,d1,d2,tb,Nb,slSep,slThick,maxB1,gSlew,n,'quadmod');
 
% Design a phase-optimized MB pulse (Malik)
[rfMBamp,gMBamp,paramsMBamp] = dz_mbrf(pulseType,d1,d2,tb,Nb,slSep,slThick,maxB1,gSlew,n,'ampmod');

% Design a time-shifted MB pulse (Auerbach)
[rfMBtime,gMBtime,paramsMBtime] = dz_mbtimeshiftrf(pulseType,d1,d2,tb,Nb,slSep,slThick,maxB1,gSlew,n);

% Design a time-shifted MB pulse with phase optimization (Auerbach)
% This doesn't buy you much compared to time-shifting alone, and takes a
% lot of computation
%[rfMBtimePhs,gMBtimePhs,paramsMBtimePhs] = dz_mbtimeshiftrf(pulseType,d1,d2,tb,Nb,slSep,slThick,maxB1,gSlew,true);

% Design a root-flipped MB pulse (Sharma)
if ~isempty(which('dzrootflipmb'))
    bandSep = slSep/slThick*tb; % integer band separation
    [rfMBRootFlip,tbRootFlip] = dzrootflipmb(n,tb,d1,d2,bandSep,Nb,'singleSE');
    paramsMBRootFlip.dt = max(abs(rfMBRootFlip))/(gamma*maxB1);
    rfMBRootFlip = rfMBRootFlip./max(abs(rfMBRootFlip))*maxB1;
    paramsMBRootFlip.gAmp = (tbRootFlip/(n*paramsMBRootFlip.dt))/(gamma/2/pi*slThick/10);
    paramsMBRootFlip.nRampPts = ceil(paramsMBRootFlip.gAmp/(paramsMBRootFlip.dt/2*gSlew));
    gMBRootFlip = [paramsMBRootFlip.gAmp*(0:paramsMBRootFlip.nRampPts-1)'/paramsMBRootFlip.nRampPts;...
        paramsMBRootFlip.gAmp*ones(n,1);paramsMBRootFlip.gAmp*(paramsMBRootFlip.nRampPts-1:-1:0)'/paramsMBRootFlip.nRampPts];
    rfMBRootFlip = [zeros(paramsMBRootFlip.nRampPts,1);rfMBRootFlip(:);zeros(paramsMBRootFlip.nRampPts,1)];
else
    disp 'Couldn''t find Root-Flipped MB pulse design, skipping...'
end

%% Simulate refocusing profiles

disp 'Simulating MB refocusing profiles...'

% Conventional MB
[~,bMB] = blochsim_simult(rfMB(:)*paramsMB.dt*gamma,gMB(:)*paramsMB.dt*gamma,zSim);
bMB = bMB.^2; % to get refocusing profile

% phase-optimized MB (Wong)
[~,bMBphs] = blochsim_simult(rfMBphs(:)*paramsMBphs.dt*gamma,gMBphs(:)*paramsMBphs.dt*gamma,zSim);
bMBphs = bMBphs.^2; % to get refocusing profile

% phase-optimized MB (Quadratic)
[~,bMBquad] = blochsim_simult(rfMBquad(:)*paramsMBquad.dt*gamma,gMBquad(:)*paramsMBquad.dt*gamma,zSim);
bMBquad = bMBquad.^2; % to get refocusing profile

% phase-optimized MB (Malik)
[~,bMBamp] = blochsim_simult(rfMBamp(:)*paramsMBamp.dt*gamma,gMBamp(:)*paramsMBamp.dt*gamma,zSim);
bMBamp = bMBamp.^2; % to get refocusing profile

% time-shifted MB (Auerbach)
[~,bMBtime] = blochsim_simult(rfMBtime(:)*paramsMBtime.dt*gamma,gMBtime(:)*paramsMBtime.dt*gamma,zSim);
bMBtime = bMBtime.^2; % to get refocusing profile

if exist('rfMBRootFlip','var')
    % Root-flipped MB (Sharma)
    [~,bMBRootFlip] = blochsim_simult(rfMBRootFlip(:)*paramsMBRootFlip.dt*gamma,gMBRootFlip(:)*paramsMBRootFlip.dt*gamma,zSim);
    bMBRootFlip = bMBRootFlip.^2; % to get refocusing profile
end

%% Plot pulses and refocusing profiles

disp 'Plotting MB pulses and profiles...'

% time vectors for RF waveform plots
tMB = ((0:length(rfMB)-1)*paramsMB.dt-length(rfMB)*paramsMB.dt/2)*1000;
tMBphs = ((0:length(rfMBphs)-1)*paramsMBphs.dt-length(rfMBphs)*paramsMBphs.dt/2)*1000;
tMBquad = ((0:length(rfMBquad)-1)*paramsMBquad.dt-length(rfMBquad)*paramsMBquad.dt/2)*1000;
tMBamp = ((0:length(rfMBamp)-1)*paramsMBamp.dt-length(rfMBamp)*paramsMBamp.dt/2)*1000;
tMBtime = ((0:length(rfMBtime)-1)*paramsMBtime.dt-length(rfMBtime)*paramsMBtime.dt/2)*1000;
if exist('rfMBRootFlip','var')
    tMBRootFlip = ((0:length(rfMBRootFlip)-1)*paramsMBRootFlip.dt-length(rfMBRootFlip)*paramsMBRootFlip.dt/2)*1000;
end

% All phase-optimized
figure
subplot(411)
plot(tMB,rfMB);
xlabel 'ms',ylabel 'Gauss'
axis([min([tMB tMBphs tMBquad tMBamp]) max([tMB tMBphs tMBquad tMBamp]) [-1 1]*maxB1]);
title(sprintf('Conventional MB: %0.3g ms duration',length(rfMB)*paramsMB.dt*1000));
subplot(412)
plot(tMBphs,abs(rfMBphs));
xlabel 'ms',ylabel 'Gauss'
axis([min([tMB tMBphs tMBquad tMBamp]) max([tMB tMBphs tMBquad tMBamp]) 0 maxB1]);
title(sprintf('Phase-optimized MB (Wong): %0.3g ms duration',length(rfMBphs)*paramsMBphs.dt*1000));
subplot(413)
plot(tMBquad,abs(rfMBquad));
xlabel 'ms',ylabel 'Gauss'
axis([min([tMB tMBphs tMBquad tMBamp]) max([tMB tMBphs tMBquad tMBamp]) 0 maxB1]);
title(sprintf('Phase-optimized MB (Quadratic): %0.3g ms duration',length(rfMBquad)*paramsMBquad.dt*1000));
subplot(414)
plot(tMBamp,rfMBamp);
xlabel 'ms',ylabel 'Gauss'
axis([min([tMB tMBphs tMBquad tMBamp]) max([tMB tMBphs tMBquad tMBamp]) [-1 1]*maxB1]);
title(sprintf('Phase-optimized MB (Malik): %0.3g ms duration',length(rfMBamp)*paramsMBamp.dt*1000));

% Wong versus time-shifted and root-flipped
figure
subplot(311)
plot(tMBphs,abs(rfMBphs));
xlabel 'ms',ylabel 'Gauss'
axis([min([tMBphs tMBtime tMBRootFlip]) max([tMBphs tMBtime tMBRootFlip]) 0 maxB1]);
title(sprintf('Phase-optimized MB (Wong): %0.3g ms duration',length(rfMBphs)*paramsMBphs.dt*1000));
subplot(312)
plot(tMBtime,abs(rfMBtime));
xlabel 'ms',ylabel 'Gauss'
axis([min([tMBphs tMBtime tMBRootFlip]) max([tMBphs tMBtime tMBRootFlip]) 0 maxB1]);
title(sprintf('Time-shifted MB (Auerbach): %0.3g ms duration',length(rfMBtime)*paramsMBtime.dt*1000));
if exist('rfMBRootFlip','var')
    subplot(313)
    plot(tMBRootFlip,abs(rfMBRootFlip));
    xlabel 'ms',ylabel 'Gauss'
    axis([min([tMBphs tMBtime tMBRootFlip]) max([tMBphs tMBtime tMBRootFlip]) 0 maxB1]);
    title(sprintf('Root-Flipped MB (Sharma): %0.3g ms duration',length(rfMBRootFlip)*paramsMBRootFlip.dt*1000));
end

figure
subplot(411)
plot(zSim,abs(bMB)*100);
xlabel 'cm',ylabel 'Refocusing Efficiency (%)'
axis([zSim(1) zSim(end) 0 100]);
title 'Conventional MB'
subplot(412)
plot(zSim,abs(bMBphs)*100);
title 'Phase-optimized MB (Wong)'
xlabel 'cm',ylabel 'Refocusing Efficiency (%)'
axis([zSim(1) zSim(end) 0 100]);
subplot(413)
plot(zSim,abs(bMBquad)*100);
title 'Phase-optimized MB (Quadratic)'
xlabel 'cm',ylabel 'Refocusing Efficiency (%)'
axis([zSim(1) zSim(end) 0 100]);
subplot(414)
plot(zSim,abs(bMBamp)*100);
title 'Phase-optimized MB (Malik)'
xlabel 'cm',ylabel 'Refocusing Efficiency (%)'
axis([zSim(1) zSim(end) 0 100]);

figure
subplot(311)
plot(zSim,abs(bMBphs)*100);
title 'Phase-optimized MB (Wong)'
xlabel 'cm',ylabel 'Refocusing Efficiency (%)'
axis([zSim(1) zSim(end) 0 100]);
subplot(312)
plot(zSim,abs(bMBtime)*100);
xlabel 'cm',ylabel 'Refocusing Efficiency (%)'
axis([zSim(1) zSim(end) 0 100]);
title 'Time-shifted MB (Auerbach)'
if exist('rfMBRootFlip','var')
    subplot(313)
    plot(zSim,abs(bMBRootFlip)*100);
    xlabel 'cm',ylabel 'Refocusing Efficiency (%)'
    axis([zSim(1) zSim(end) 0 100]);
    title 'Root-Flipped MB (Sharma)'
end

