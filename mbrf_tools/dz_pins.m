function [rf,g,params] = dz_pins(pulseType,d1,d2,tb,slSep,slThick,maxB1,gMax,gSlew,dt,minRFDur,multiPINS,Nb,minPhs)

% function [rf,g,params] = dz_pins(pulseType,d1,d2,tb,slSep,slThick,maxB1,gMax,gSlew,dt,minRFDur,multiPINS,Nb,minPhs)
% 
%   Design a conventional or min-duration PINS pulse.
% 
%   out: 
%       rf,g: RF and gradient pulse waveforms
%       params: RF parameter structure, containing
%               dur: duration of pulse (seconds)
%               gMoment: gradient moment for each blip(Gauss/cm*seconds)
%               dt: dwell time (seconds)
%               NPulses: Number of subpulses
%               gRefMoment: refocusing gradient moment
%               M: MultiPINS mixing ratio (if MultiPINS)
%   in: 
%       pulseType: For SLR ('se','ex','sat','inv')
%       d1,d2: For SLR (passband and stopband ripple)
%       tb: Time-bandwidth product
%       slSep: Slice separation in mm
%       slThick: Slice thickness in mm 
%       maxB1: max RF amplitude in gauss
%       gMax: max gradient amplitude in Gauss/cm
%       gSlew: max gradient slew in Gauss/cm/s
%       dt: dwell time in seconds
%       minRFdur: switch to produce a min-duration pulse by scaling the subpulses up if they are less than maxB1 (optional)
%       multiPINS: switch to produce a min-energy multiPINS pulse (optional)
%       Nb: Number of bands (required for multiPINS) 
%       minPhs: design a shorter minimum phase pulse
% 
%   Will Grissom, Vanderbilt University, 2015

if ~exist('minRFDur','var')
    minRFDur = false;
end
if ~exist('multiPINS','var')
    multiPINS = false;
end
if multiPINS && minRFDur
    error 'min-duration not compatible with multiPINS'
end
if multiPINS
    if ~exist('Nb','var')
        error 'Nb must be provided for multiPINS'
    end
    if ~rem(Nb,2)
        error 'Nb must be odd for multiPINS'
    end
end
if ~exist('minPhs','var')
    minPhs = false;
end
if minPhs
    % assumes spin echo
    fType = 'min';
    dinfmin = 1/2*dinf(2*(d1/4),sqrt(d2)^2/2); % d-infinity for a min-phase pulse with beta ripples (d1,d2)
    dinflin = dinf(d1/4,sqrt(d2));      % d-infinity for a linear phase pulse with the same ripples
    tb = tb/dinflin*dinfmin; % scale TBW product so as to get the same transition
                             % width as linear phase pulse with same ripples,
                             % after scaling back to desired slice thickness. This
                             % makes comparison to other MB excitations more
                             % meaningful, since all will have same slice characteristics.
else
    fType = 'ls';
end

kzw = tb/(slThick/10); % 1/cm, width in kz-space we must go
NPulses = 2*floor(ceil(kzw/(1/(slSep/10)))/2)+1; % number of subpulses (odd)

% call SLR to get pulse
rfSoft = real(dzrf(NPulses,tb,pulseType,fType,d1,d2)); rfSoft = rfSoft(:);
if ~minPhs
    rfSoft = (rfSoft + flipud(rfSoft))/2; % make it even
end

% using nominal peak b1, determine width of hard pulses
gambar = 4258; % Hz/g

% design the blip trapezoid
gArea = 1/(slSep/10)/gambar; % g/cm, k-area of each blip
gzblip = dotrap(gArea,gMax,gSlew,dt).';

if ~minRFDur
    
    % matched-duration RF subpulses
    hpw = ceil(max(abs(rfSoft))./(2*pi*gambar*maxB1*dt));
    
    % interleave RF subpulses with gradient blips to form full pulses
    rf = kron(rfSoft(1:end-1),[ones(hpw,1);zeros(size(gzblip))]);
    rf = [rf;rfSoft(end)*ones(hpw,1)];
    
    g = repmat([zeros(hpw,1);gzblip(:)],[NPulses-1 1]);
    g = [g;zeros(hpw,1)];
    
    if multiPINS
        
        % get a high-res pulse to interpolate onto the gradient blips with VERSE
        rfSoftHR = real(dzrf(128,tb,pulseType,fType,d1,d2));
        % interpolate 
        rfSoftHR = interp1((0:127)./127,rfSoftHR,(0:8191)./8191,'spline',0);
        
        % make it into an MB pulse (depends on kzw)
        kz = -kzw/2:kzw/8191:kzw/2;
        zLocs = (-(Nb-1)/2:(Nb-1)/2)*slSep/10;
        rfMB = sum(cos(2*pi*kz(:)*zLocs(:)'),2).*rfSoftHR(:);
        % VERSE the MB pulse onto the gradient blips
        rfMB = verse(g(g ~= 0),rfMB);
        
        % calculate the SAR-optimal mixing ratio: 
        % energy = M^2*rfMBenergy + (1-M)^2*PINSenergy
        % so denergy/dM = 2*M*rfMBenergy + 2*(M-1)*PINSenergy 
        % this is 0 at optimum,
        % where M = PINSenergy/(rfMBenergy + PINSenergy)
        rf = rf./sum(rf(:));rfMB = rfMB./sum(rfSoftHR(:));
        M = norm(rf)^2/(norm(rfMB)^2 + norm(rf)^2);
        
        % apply mixing ratio to get multiPINS pulse
        rf = (1-M)*rf;
        %rf(g == 0) = 0;
        rf(g ~= 0) = M*rfMB;
        
        params.M = M; % return mixing ratio
        
    end
    
else
    
    % matched-amplitude RF subpulses
    rf = [];g = [];
    for ii = 1:NPulses
        % calculate number of samples so that this subpulse
        % is as close as possible to maxB1, without exceeding it
        hpw = ceil(abs(rfSoft(ii))./(2*pi*gambar*maxB1*dt));
        % calculate how much we have to attenuate this pulse
        % to account for the fact that with this dwell time
        % we couldn't hit the target flip exactly
        scalfact = rfSoft(ii)/(2*pi*gambar*maxB1*dt*hpw);
        % stitch on the subpulse
        rf = [rf; scalfact*ones(hpw,1)];
        g = [g; zeros(hpw,1)];
        if ii < NPulses
            rf = [rf;zeros(size(gzblip))];
            g = [g;gzblip(:)];
        end
    end
    
end

rf = rf./(sum(rf(:))*2*pi*gambar*dt)*sum(rfSoft); % convert to gauss

params.dur = length(rf)*dt; % duration of pulse (seconds)
params.gMoment = sum(gzblip)*dt; % gradient moment (Gauss/cm*seconds)
params.dt = dt;
params.NPulses = NPulses;
params.gRefMoment = NPulses/2*params.gMoment; % refocusing gradient moment

