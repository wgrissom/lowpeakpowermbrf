function [rf,g,params] = dz_mbtimeshiftrf(pulseType,d1,d2,tb,Nb,slSep,slThick,maxB1,gSlew,n,phsOpt)

% function [rfMB,gMB,params] = dz_mbtimeshiftrf(pulseType,d1,d2,tb,Nb,slSep,slThick,maxB1,gSlew,phsOpt)
%
%   Design a time-shifted multiband pulse with a given peak B1, with optional inter-band phases.
%
%   out:
%       rf,g: RF and gradient pulse waveforms
%       params: Parameter structure, containing
%               gRefMoment: refocusing gradient area
%               dt: dwell time
%               params.gAmp = gAmp; % gradient plateau amplitude (Gauss/cm)
%               params.nRampPts = nRampPts; % number of time points in the gradient ramps
%               dtMax: ~ maximum dwell time the pulse could be interpolated to without aliasing
%
%   in:
%       pulseType: For SLR ('se','ex','sat','inv')
%       d1,d2: For SLR (passband and stopband ripple)
%       tb: Time-bandwidth product
%       Nb: number of bands
%       slSep: Slice separation in mm
%       slThick: Slice thickness in mm
%       maxB1: max RF amplitude in gauss
%       gSlew: max gradient slew in Gauss/cm/s
%       n: number of time points in base pulse
%       phsOpt: optionally optimize inter-band phase shifts
%               (default is false; typically saves less than 100 us and requires a lot of computation)
%
%   Will Grissom, Vanderbilt University, 2015

if ~exist('phsOpt','var')
    phsOpt = false;
end
if ~exist('n','var')
    n = 8192; % number of time points in high-res pulse
end

gambar = 4258; % Hz/Gauss

% get a high-res pulse
rf1bLR = real(dzrf(128,tb,pulseType,'ls',d1,d2));
% interpolate
rf1b = interp1((0:127)./127,rf1bLR,(0:n-1)./(n-1),'spline',0);
rf1b = rf1b./sum(rf1b)*sum(rf1bLR);

% calculate and apply the multiband modulation function
bandSep = slSep/slThick*tb; % band separation (integer)
B = exp(1i*2*pi/n*(-n/2:n/2-1)'*((0:Nb-1)-(Nb-1)/2)*bandSep);
RF = B.*repmat(rf1b(:),[1 Nb]); % apply multiband modulation function to get multiband RF

% find the best time shift
[rf,params.shift,params.phs] = findTimeShift(RF,phsOpt);

% calculate the dwell time for the peak b1
dt = max(abs(rf))/(maxB1*2*pi*gambar); % seconds

% convert to Gauss
rf = rf./max(abs(rf))*maxB1; 

% calculate the gradient waveform amplitude
gAmp = tb/(dt*n)/gambar/(slThick/10); % Gauss/cm

% put ramps on each end
nRampPts = ceil(gAmp/(dt/2*gSlew)); % number of time points on the ramps
g = [gAmp*(0:nRampPts-1)'/nRampPts;gAmp*ones(length(rf),1);gAmp*(nRampPts-1:-1:0)'/nRampPts];
rf = [zeros(nRampPts,1);rf;zeros(nRampPts,1)];

params.nRampPts = nRampPts; % number of points in gradient ramps
params.gAmp = gAmp; % gradient plateau amplitude
params.shift = params.shift*dt; % convert shift per band from integer to seconds
params.dt = dt; % dwell time

function [minDurRF,bestShift,bestPhs] = findTimeShift(RF,phsopt)

% loop over integer time shifts to minimize peak B1
% matrix RF is # time points x # bands, contains RF pulses for each band
% optionally band-specific phase optimization can be performed

% script should loop through delays (in terms of samples) and find the one
% that minimizes duration*peakb1

[Nt,Nb] = size(RF);

shiftperband = 0:2:floor(Nt/2); % reasonable upper bound for TB >= 3

bestShift = 0;
mindur = Inf;
bestPhs = 0;

for ii = 1:length(shiftperband)
    
    % embed pulses into larger matrix, in the right places
    RFt = zeros(Nt+(Nb-1)*shiftperband(ii),Nb);
    for jj = 1:Nb
        si = 1+(jj-1)*shiftperband(ii);
        RFt(si:si+Nt-1,jj) = RF(:,jj);
    end
    
    % phase-optimize bands to further reduce peak rf
    phs = zeros(Nb-1,1);
    if phsopt
        phsoptrf_max = Inf;
        for jj = 1:10
            phsi = 2*pi*rand(Nb-1,1);
            tmp = fminsearch(@(x)max(abs(RFt*exp(1i*[0;x]))),phsi);
            if max(abs(sum(RFt*exp(1i*[0;tmp]),2))) < phsoptrf_max
                phs = tmp;
                phsoptrf_max = max(abs(sum(RFt*exp(1i*[0;tmp]),2)));
            end
        end
    end
    
    % sum the pulses
    rf = sum(RFt*exp(1i*[0;phs]),2);
    
    % calculate duration corresponding to peak limit
    dur = length(rf)*max(abs(rf));
    
    if dur < mindur
        minDurRF = rf;
        mindur = dur;
        bestShift = shiftperband(ii);
        bestPhs = [0;phs];
    end
    
    if rem(ii,1000) == 0
        fprintf('Time-shift optimization iter %d of %d.\n',ii,length(shiftperband));
    end
    
end
