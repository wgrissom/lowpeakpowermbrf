This repository provides MATLAB code and documents related to Will Grissom's lecture on low-peak-power multiband pulses at the 2015 ISMRM Workshop on Simultaneous Multislice Imaging. Running the code will require John Pauly's SLR pulse design tools: 

http://rsl.stanford.edu/research/software.html 

and Will Grissom's Root-flipped multiband pulse design code: 

https://bitbucket.org/wgrissom/root-flipped-multiband-refocusing-pulses. 

A copy of the talk slides are also given, and you can watch a recorded version of the talk here:

https://youtu.be/Hyfe9rEiNxg

Please let me know if you have any feedback or suggestions! will.grissom@vanderbilt.edu

The files are:

* dz_pins.m: Design PINS and MultiPINS pulses
* dz_mbrf.m: Design conventional multiband pulses with optional optimal phases
* dz_mbtimeshiftrf.m: Design time-shifted multiband pulses
* mbphstab.m: Get optimal phases for low-peak-power multiband pulses (used by dz_mbrf.m)
* test_dz_mbrf.m: Design multiband pulses using the various methods and compare
* test_dz_pins.m: Design PINS/MultiPINS pulses using the various methods and compare
* blochsim_simult.m: Bloch simulation for evaluating slice profiles in the test_* functions

Will Grissom, Vanderbilt University, 2015